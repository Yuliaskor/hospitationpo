package com.example.hospitationWeb;

import com.example.hospitationWeb.model.Courses;
import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import com.example.hospitationWeb.repository.CoursesRepository;
import com.example.hospitationWeb.repository.FormRepository;
import com.example.hospitationWeb.repository.HospitationRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class HospitationWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(HospitationWebApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(
                            FormRepository formRepository,
                             CoursesRepository coursesRepository,
                             HospitationRepository hospitationRepository

                             ) {
        return args -> {

            Courses courses1 = new Courses(" Programowanie system. webowych", "INZ004361L ", "15", "Poniedziałek, 1705-1845, D-2, s. 127a", "Mgr inż. Arkadiusz Warzyński");
            coursesRepository.save(courses1);

            Courses courses2 = new Courses( "Cyberbezpieczeństwo", "INZ004358W ", "150", "Poniedziałek, 915-1100, A-1, s. 329", "Dr hab. inż. Grzegorz Kołaczek");
            coursesRepository.save(courses2);

            Courses courses3 = new Courses("Baza danych Oracle-programow.", "INZ004364P ", "15", "Poniedziałek, 1115-1300, D-2, s. 127c", "Dr inż. Zbigniew Staszak");
            coursesRepository.save(courses3);

            Courses courses4 = new Courses(" Aplik mobilne na platformę IOS", "INZ004361L ", "15", "\tPoniedziałek, 1315-1500, C-6, s. 130", "Dr inż. Krzysztof Waśko");
            coursesRepository.save(courses4);

            Courses courses5 = new Courses(" Projektowanie oprogramowania", "INZ004360W", "15", "Poniedziałek, 1705-1845, D-2, s. 127a", "Dr inż. Bogumiła Hnatkowska");
            coursesRepository.save(courses5);

            Hospitation hospitation = new Hospitation("hospitujacy", "hospitujacyDrugi", "hospitowany","20.05.2022", StatusProtokol.NIE_WYPELNIONY.getName(), "Letni2022" );
            hospitation.setCourses(courses1);
            hospitationRepository.save(hospitation);

            Hospitation hospitation1 = new Hospitation("hospitujacy",  "hospitujacyDrugi","hospitowany","21.05.2022", StatusProtokol.NIE_WYPELNIONY.getName(), "Letni2022" );
            hospitationRepository.save(hospitation1);

            Form form = new Form("0","jest", "nie ma", "Dr inż. Bogumiła Hnatkowska","Projektowanie opragramowania", "INZ004360W","Project","stacjonarno","stacjo","Poniedziałek, 1705-1845, D-2, s. 127a","stacjo","Bardzo bardzo dobrze","Wnioski",true,true,true,true,true,true, 5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5,5);

            formRepository.save(form);

            Hospitation hospitationArch = new Hospitation("hospitujacy",  "hospitujacyDrugi","hospitowany", StatusProtokol.ARCHIWOWANY.getName(), form  );
            hospitationRepository.save(hospitationArch);


        };

    }

}
