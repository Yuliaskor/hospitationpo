package com.example.hospitationWeb.repository;

import com.example.hospitationWeb.model.AcademicTeacher;
import com.example.hospitationWeb.model.Courses;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AcademicTeacherRepository extends JpaRepository<AcademicTeacher, Long> {
}
