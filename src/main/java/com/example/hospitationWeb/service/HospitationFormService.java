package com.example.hospitationWeb.service;

import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import com.example.hospitationWeb.repository.FormRepository;
import com.example.hospitationWeb.repository.HospitationRepository;
import org.springframework.stereotype.Service;

@Service
public class HospitationFormService {
    private final FormRepository formRepository;

    private final HospitationRepository hospitationRepository;

    public HospitationFormService(FormRepository formRepository, HospitationRepository hospitationRepository) {
        this.formRepository = formRepository;
        this.hospitationRepository = hospitationRepository;
    }

    public void saveForm(Form form, Long hospitationId, StatusProtokol status){
        Hospitation hospitation = hospitationRepository.findById(hospitationId).get();
        hospitation.setForm(form);
        hospitation.setStatus(status.getName());

        formRepository.save(form);

        hospitationRepository.save(hospitation);
    }

    public void changeStatus(Long hospitationId, StatusProtokol statusProtokol){
        Hospitation hospitation = hospitationRepository.getOne(hospitationId);
        hospitation.setStatus(statusProtokol.getName());
        hospitationRepository.save(hospitation);
    }

}
