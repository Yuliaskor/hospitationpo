package com.example.hospitationWeb.web;

import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import com.example.hospitationWeb.repository.HospitationRepository;
import com.example.hospitationWeb.service.HospitationFormService;
import com.example.hospitationWeb.utils.FormValidator;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import com.example.hospitationWeb.utils.GeneratePdfReport;

import org.springframework.http.HttpHeaders;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("")
public class HospitationFormController {

    private final String hospitationFormPage = "/forms/hospitationForm";
    private final String hospitationResultPage = "/admin/hospitationResult";

    private final HospitationRepository hospitationRepository;

    private final FormValidator formValidator;
    private HospitationFormService hospitationFormService;


    public HospitationFormController(HospitationRepository hospitationRepository, FormValidator formValidator, HospitationFormService hospitationFormService) {
        this.hospitationRepository = hospitationRepository;
        this.formValidator = formValidator;
        this.hospitationFormService = hospitationFormService;
    }

    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_HOSPITUJACY')")
    @GetMapping("/hospitationForm/")
    public String getHospitationForm(@RequestParam(name = "id") String id, Model model) {
        model.addAttribute("id", id);
        model.addAttribute("form", new Form());
        return hospitationFormPage;
    }
    @GetMapping("/hospitationFormAproved/")
    public String getHospitationFormAproved(@RequestParam(name = "id") String id, Model model) {
        hospitationFormService.changeStatus( Long.valueOf(id), StatusProtokol.POTWIERDZONY);
        model.addAttribute("hospitations", hospitationRepository.findAll());
        return hospitationResultPage;
    }
    @PostMapping("/addToForm")
    public String registration(@ModelAttribute("form") Form userForm, BindingResult bindingResult) {
       formValidator.validate(userForm, bindingResult);

        if (bindingResult.hasErrors()) {
            return hospitationFormPage;
        }

        hospitationFormService.saveForm(userForm, 1L, StatusProtokol.OCZEKUJE_NA_POTWIERDZENIE);

        return "redirect:/getHospitationPlan";
    }

    //    @GetMapping("/hospitation/export/pdf")
//    public void exportToPDF(HttpServletResponse response) throws DocumentException, IOException {
//        response.setContentType("application/pdf");
//        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
//        String currentDateTime = dateFormatter.format(new Date());
//
//        String headerKey = "Content-Disposition";
//        String headerValue = "attachment; filename=users_" + currentDateTime + ".pdf";
//        response.setHeader(headerKey, headerValue);
//
//        List<User> listUsers = service.listAll();
//
//        UserPDFExporter exporter = new UserPDFExporter(listUsers);
//        exporter.export(response);
//
//    }
    @RequestMapping(value = "/hospitation/pdfreport/Archiw", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> hospitationPdf() throws IOException {
        Optional<Hospitation> hospitation = hospitationRepository.findById(3L);
        ByteArrayInputStream bis = null;
        if (hospitation.orElse(null) != null) {
            bis = GeneratePdfReport.hospitationReport(hospitation.orElse(null));
        } else {
            return null;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }

    @RequestMapping(value = "/hospitation/pdfreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> hospitationPdfA() throws IOException {
        Optional<Hospitation> hospitation = hospitationRepository.findById(1L);
        ByteArrayInputStream bis = null;
        if (hospitation.orElse(null) != null) {
            bis = GeneratePdfReport.hospitationReport(hospitation.orElse(null));
        } else {
            return null;
        }

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }
    @RequestMapping(value = "/hospitationRaport/pdfreport", method = RequestMethod.GET, produces = MediaType.APPLICATION_PDF_VALUE)
    public ResponseEntity<InputStreamResource> hospitationRaportPdf() throws IOException {

        List<Hospitation> hospitation = hospitationRepository.findAllBySemestr("Letni2022");
        ByteArrayInputStream bis = GeneratePdfReport.hospitationSemestrReport(hospitation);


        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=citiesreport.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(bis));
    }


    @GetMapping("/hospitationResult")
    public String getHospitationResult(Model model) {
        model.addAttribute("hospitations", hospitationRepository.findAll());

        //  model.addAttribute("form", new Form());

        return hospitationResultPage;
    }

    @GetMapping("/hospitationArchiwum")
    public String getHospitationArchiwum(Model model) {
        model.addAttribute("hospitations", hospitationRepository.findAllByStatus(StatusProtokol.ARCHIWOWANY.getName()));

        //  model.addAttribute("form", new Form());

        return hospitationResultPage;
    }

}
