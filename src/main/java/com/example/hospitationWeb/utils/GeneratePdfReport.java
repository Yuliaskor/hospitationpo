package com.example.hospitationWeb.utils;

import com.example.hospitationWeb.model.Hospitation;
import com.google.common.base.Strings;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

public class GeneratePdfReport {

    public static final String FONT_BOLD = "static/fonts/AbhayaLibre-Bold.ttf";
    public static final String FONT_EXTRA_BOLD = "static/fonts/AbhayaLibre-ExtraBold.ttf";
    public static final String FONT_SEMI_BOLD = "static/fonts/AbhayaLibre-SemiBold.ttf";
    public static final String FONT_MEDIUM = "static/fonts/AbhayaLibre-Medium.ttf";
    public static final String FONT_REGULAR = "static/fonts/AbhayaLibre-Regular.ttf";
    public static final String IMG_PWR_DESC = "src/main/resources/static/image/img.png";

    public static final Font fontBold =
            FontFactory.getFont(FONT_BOLD, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 26);
    public static final Font fontBoldForHeader =
            FontFactory.getFont(FONT_BOLD, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14);
    public static final Font fontMedium =
            FontFactory.getFont(FONT_MEDIUM, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 20);
    public static final Font fontRegular =
            FontFactory.getFont(FONT_REGULAR, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14);
    public static final Font fontExtraBold =
            FontFactory.getFont(FONT_EXTRA_BOLD, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14);
    public static final Font fontSemiBold =
            FontFactory.getFont(FONT_SEMI_BOLD, BaseFont.IDENTITY_H, BaseFont.EMBEDDED, 14);


    public static ByteArrayInputStream hospitationReport(Hospitation hospitation) {

        Document document = new Document();
        addMetaData(document);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            PdfWriter.getInstance(document, out);
            document.open();


            Paragraph preface = new Paragraph();
            addEmptyLine(preface, 9);
            Image img = Image.getInstance(IMG_PWR_DESC);
            img.setAlignment(Element.ALIGN_CENTER);
            preface.add(img);

            preface.add(new Paragraph("PROTOKÓŁ Z HOSPITACJI ZAJĘĆ", fontBold));
            document.add(Chunk.NEWLINE);

            addEmptyLine(preface, 1);
            preface.add(new Paragraph("Wrocław, " + new Date(), fontMedium));
            addEmptyLine(preface, 3);
            preface.add(new Paragraph("Zespół hospitujący: ", fontRegular));
            preface.add(new Paragraph("Id pierwszego hospitującego - "
                    + hospitation.getHospitujacyPierwszyId(), fontRegular));
            preface.add(new Paragraph("Id drugiego hospitującego - "
                    + hospitation.getHospitujacyDrugiId(), fontRegular));


            addEmptyLine(preface, 8);

            document.add(preface);
            document.newPage();

            Anchor anchor = new Anchor("Informacje wstępne", fontMedium);
            Chapter introPart = new Chapter(new Paragraph(anchor), 1);
            Paragraph subIntroPart = new Paragraph(
                    "Prowadzący zajęcia/Jednostka organizacyjna: "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getProwadzacy()
                            .toUpperCase(), 50, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Nazwa kursu/kierunek studiów: "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getNazwaKursa()
                            .toUpperCase(), 80, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Kod kursu: "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getKod()
                            .toUpperCase(), 80, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Forma dydaktyczna: " +
                            Strings.padStart(hospitation
                                    .getForm()
                                    .getFormaDydaktyczna()
                                    .toUpperCase(), 80, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Sposób realizacji (tradycyjny, zdalny): "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getSposobRealizacji()
                            .toUpperCase(), 80, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Stopień i forma studiów: "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getFormaStudiow()
                            .toUpperCase(), 80, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Semestr: "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getSemestr()), 100, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Miejsce i termin zajęć (sala, budynek , dzień tygodnia, godziny): "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getTermin()
                            .toUpperCase(), 10, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);
            subIntroPart = new Paragraph(
                    "Środowisko realizacji zajęć (np. ZOOM, MS Teams, Google Meet): "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getSrodowisko()
                            .toUpperCase(), 10, '.'),
                    fontRegular
            );
            introPart.addSection(subIntroPart);

            document.add(introPart);

            anchor = new Anchor("Ocena formalna zajęć", fontMedium);
            Chapter secondPart = new Chapter(new Paragraph(anchor), 2);
            Paragraph subSecondPart = new Paragraph(
                    "Czy zajęcia rozpoczęły się punktualnie? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzyZajeciaRozpoczelySiePunktualnie() ? "TAK" : "NIE", 50, '.'),
                    fontRegular
            );
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Czy sprawdzono obecność studentów? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzySprawdzonoObecnoscStudentow() ? "TAK" : "NIE", 50, '.'),
                    fontRegular
            );
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Czy sala i jej wyposażenie są przystosowane do formy? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzySalaJestDostosowana() ? "TAK" : "NIE", 50, '.'),
                    fontRegular
            );
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Czy prowadzący weryfikuje, że studenci go słyszą i widzą przekaz wideo? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzyProwadzacywWeryfikuje() ? "TAK" : "NIE", 50, '.'),
                    fontRegular
            );
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Treść zajęć jest zgodna z programem kursu i umożliwia osiągnięcie założonych " +
                            "efektów uczenia się ujętych w Karcie Przedmiotu? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzyTrescZgodnaZProgramemKursu() ? "TAK" : "NIE", 50, '.'),
                    fontRegular
            );
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Czy prowadzący umożliwił studentom dostęp do informacji i materiałów dydaktycznych " +
                            "przez środki komunikacji elektronicznej? "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getCzyUmozliwilDostep() ? "TAK" : "NIE", 50, '.'), fontRegular);
            secondPart.addSection(subSecondPart);
            subSecondPart = new Paragraph(
                    "Inne uwagi, wnioski i zalecenia dotyczące formalnej strony zajęć: "
                            + Strings.padStart(hospitation
                            .getForm()
                            .getInneUwagi(), 10, '.'), fontRegular);
            secondPart.addSection(subSecondPart);

            document.add(secondPart);

            anchor = new Anchor("Ocena merytoryczna i metodyczna przeprowadzonych zajęć", fontMedium);
            Chapter thirdPart = new Chapter(new Paragraph(anchor), 3);
            addEmptyLine(preface, 1);

            Paragraph subThirdPart = new Paragraph(
                    "Przedstawił temat, cel i zakres zajęć "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getTematCel()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Wyjaśniał w zrozumiały sposób omawiane zagadnienia "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getWyjasnienie()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Realizował zajęcia z zaangażowaniem "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getZaangazowanie()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Inspirował studentów do samodzielnego myślenia "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getInspiracja()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Udzielał merytorycznie poprawnych odpowiedzi na pytania studentów "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPoprawneOdpowiedzi()), 20, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Stosował środki dydaktyczne adekwatnie do celów i treści zajęć"
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getSrodkiDydaktyczne()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Posługiwał się poprawnym językiem "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPoprawnyJezyk()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Panował nad dynamiką grupy "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getDynamikaGrupy()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Tworzył pozytywną atmosferę na zajęciach "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getAtmosfera()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Sprawnie posługiwał się technicznymi środkami przekazu wiedzy "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getTechnicznesrodkiprzekazaniawiedzy()), 20, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Przekazywał aktualną wiedzę  " + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getAktualnaWiedza()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Przedstawiał materiał, który był przygotowany i uporządkowany "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPrzygotowanie()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Wykazał się umiejętnościami w zakresie nauczania "
                            + Strings.padStart(String.valueOf(hospitation.
                            getForm()
                            .getUmiejetnosciNauczania()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Poprawnie dobierał przykłady – pod względem treści "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPrzyklady()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Tempo prowadzonych zajęć dostosował do możliwości studentów "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getTempo()), 20, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Był przygotowany merytorycznie do danej formy zajęć "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPrzygotowanie()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Jasno określił zadania dla studentów "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getZadania()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Odpowiednio rozplanował czas zajęć "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getPlanowanie()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Kontrolował umiejętności zdobywane w trakcie zajęć, komentował i korygował wypowiedzi "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getKomentarzy()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);
            subThirdPart = new Paragraph(
                    "Prowadził dokumentację zajęć (lista obecności, lista ocen, sprawozdania, prace kontrolne itp.) "
                            + Strings.padStart(String.valueOf(hospitation
                            .getForm()
                            .getDokumentacja()), 50, '.'),
                    fontRegular
            );
            thirdPart.addSection(subThirdPart);

            document.add(thirdPart);

            anchor = new Anchor("Ocena końcowa", fontMedium);
            Chapter finalGradePart = new Chapter(new Paragraph(anchor), 4);
            addEmptyLine(preface, 1);

            Paragraph subFinalGradePart = new Paragraph(
                    """
                              Ocenę końcową z hospitacji wystawia się na podstawie średniej oceny merytorycznej
                              i metodycznej S wyznaczonej w pkt. 3. z zastosowaniem poniższej skali.
                              Skala ocen:
                              wzorowa S >= 5,0;
                              bardzo dobra: 5,0 > S ≥ 4,5;
                              dobra: 3,5 ≤ S < 4,5;
                              dostateczna: 2,5 ≤ S < 3,5;
                              negatywna: S < 2,5
                              Zespół hospitujący może obniżyć ocenę końcową, jeżeli ocena formalna hospitowanych zajęć
                              (pkt. 2) wskazuje na rażące uchybienia.
                            """, fontRegular);
            finalGradePart.add(subFinalGradePart);

            Paragraph finalGrade = new Paragraph(
                    "Ocena końcowa " + hospitation.getForm().getKoncowa(),
                    fontMedium
            );
            finalGradePart.add(finalGrade);
            document.add(finalGradePart);

            anchor = new Anchor("Uzasadnienie oceny końcowej", fontMedium);
            Chapter proofPart = new Chapter(new Paragraph(anchor), 5);
            addEmptyLine(preface, 1);

            Paragraph proofParagraph = new Paragraph(
                    Strings.padStart(hospitation.getForm().getUzasadnienieOcenyKoncowej(), 4000, '.'),
                    fontRegular
            );
            proofPart.add(proofParagraph);
            document.add(proofPart);

            anchor = new Anchor("Wnioski i zalecenia", fontMedium);
            Chapter advicesPart = new Chapter(new Paragraph(anchor), 6);
            addEmptyLine(preface, 1);

            Paragraph advicesParagraph = new Paragraph(
                    Strings.padStart(hospitation.getForm().getWnioskiOrazZalecenia(), 4000, '.'),
                    fontRegular
            );
            advicesPart.add(advicesParagraph);
            document.add(advicesPart);
            document.close();
        } catch (DocumentException ex) {

            Logger.getLogger(GeneratePdfReport.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    public static ByteArrayInputStream hospitationSemestrReport(List<Hospitation> hospitations) {
        Document document = new Document();
        addMetaData(document);
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        try {
            PdfWriter.getInstance(document, out);
            document.open();

            Paragraph preface = new Paragraph();
            addEmptyLine(preface, 5);
            Image img = Image.getInstance(IMG_PWR_DESC);
            img.setAlignment(Element.ALIGN_CENTER);
            preface.add(img);

            Paragraph prefaceMain = new Paragraph("TABELA HOSPITACJA ZAJĘĆ", fontBold);
            prefaceMain.setAlignment(Element.ALIGN_CENTER);
            preface.add(prefaceMain);
            document.add(Chunk.NEWLINE);

            addEmptyLine(preface, 1);
            document.add(preface);


            PdfPTable table = new PdfPTable(5);
            table.setWidths(new int[]{2, 2, 1, 1, 2});
            Stream.of(
                    "Tytuł naukowy, imię i nazwisko hospitowanego",
                            "Nazwa i kod kursu",
                            "Liczba osób",
                            "Miejsce i termin",
                            "Tytuł naukowy, imię i nazwisko członka zespołu hospitującego"
                    )
                    .forEach(headerTitle -> {
                        PdfPCell header = new PdfPCell();
                        header.setBackgroundColor(BaseColor.WHITE);
                        header.setHorizontalAlignment(Element.ALIGN_CENTER);
                        header.setBackgroundColor(new BaseColor(255, 102, 102));
                        header.setVerticalAlignment(Element.ALIGN_MIDDLE);
                        header.setBorderWidth(0.5f);
                        header.setPhrase(new Phrase(headerTitle, fontBoldForHeader));
                        table.addCell(header);
                    });

            for (Hospitation hospitation : hospitations) {
                String courseName = Optional.ofNullable(hospitation
                        .getCourses()
                        .getName())
                        .orElse("NONE");
                String countOfStudents = Optional.ofNullable(hospitation
                        .getCourses().
                        getCountStudents())
                        .orElse("NONE");;
                String termAndPlace = Optional.ofNullable(hospitation
                        .getCourses()
                        .getTermAndPlace())
                        .orElse("NONE");;
                String personHospitation = Optional.ofNullable(hospitation
                        .getCourses()
                        .getPersonHospitation())
                        .orElse("NONE");;

                fontRegular.setSize(12);
                PdfPCell idCell = new PdfPCell(new Phrase(courseName, fontRegular));
                idCell.setPaddingLeft(4);
                idCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                idCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(idCell);

                PdfPCell firstNameCell = new PdfPCell(new Phrase(courseName, fontRegular));
                firstNameCell.setPaddingLeft(4);
                firstNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                firstNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(firstNameCell);

                PdfPCell lastNameCell = new PdfPCell(new Phrase(countOfStudents, fontRegular));
                lastNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                lastNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                lastNameCell.setPaddingRight(4);
                table.addCell(lastNameCell);

                PdfPCell tlastNameCell = new PdfPCell(new Phrase(termAndPlace, fontRegular));
                tlastNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                tlastNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                tlastNameCell.setPaddingRight(4);
                table.addCell(tlastNameCell);

                PdfPCell ttlastNameCell = new PdfPCell(new Phrase(personHospitation, fontRegular));
                ttlastNameCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                ttlastNameCell.setHorizontalAlignment(Element.ALIGN_CENTER);
                ttlastNameCell.setPaddingRight(4);
                table.addCell(ttlastNameCell);
            }
            document.add(table);

            document.close();
        } catch (DocumentException ex) {

            Logger.getLogger(GeneratePdfReport.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return new ByteArrayInputStream(out.toByteArray());
    }

    private static void addMetaData(Document document) {
        document.addTitle("PROTOKÓŁ Z HOSPITACJI ZAJĘĆ");
        document.addSubject("Pwr");
        document.addKeywords("Hospitation");
        document.addAuthor("Viktor");
        document.addAuthor("Yulia");
    }

    private static void addEmptyLine(Paragraph paragraph, int number) {
        for (int i = 0; i < number; i++) {
            paragraph.add(new Paragraph(" "));
        }
    }

    private static Paragraph combParagraph(String text1, Font font1, String text2, Font font2) {
        Paragraph temp = new Paragraph();
        temp.add(new Paragraph(text1, font1));
        temp.add(new Paragraph(text2, font2));
        return temp;
    }
}
