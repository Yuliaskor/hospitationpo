package com.example.hospitationWeb.utils;

import com.example.hospitationWeb.model.Form;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class FormValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Form.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
            Form form = (Form) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "prowadzacy", "NotEmpty");
//        if(form.getProwadzacy().isEmpty()){
//            errors.rejectValue("prowadzacy", "NotEmptyA");
//        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "nazwaKursa", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "kod", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "formaDydaktyczna", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sposobRealizacji", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "formaStudiow", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "termin", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "srodowisko", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "uzasadnienieOcenyKoncowej", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wnioskiOrazZalecenia", "NotEmpty");

//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "semestr", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czyZajeciaRozpoczelySiePunktualnie", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czySprawdzonoObecnoscStudentow", "NotEmpty");

//        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czySalaJestDostosowana", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czyProwadzacywWeryfikuje", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czyTrescZgodnaZProgramemKursu", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "czyUmozliwilDostep", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "koncowa", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tematCel", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "wyjasnienie", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zaangazowanie", "NotEmpty");


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "inspiracja", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "poprawneOdpowiedzi", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "srodkiDydaktyczne", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "poprawnyJezyk", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dynamikaGrupy", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "atmosfera", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "technicznesrodkiprzekazaniawiedzy", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "aktualnaWiedza", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "material", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "umiejetnosciNauczania", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "przyklady", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "tempo", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "przygotowanie", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "zadania", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "planowanie", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "komentarzy", "NotEmpty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dokumentacja", "NotEmpty");

    }
}
