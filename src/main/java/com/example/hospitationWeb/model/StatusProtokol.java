package com.example.hospitationWeb.model;

public enum StatusProtokol {

    OCZEKUJE_NA_POTWIERDZENIE("Oczekuje na potwierdzenie"),
    POTWIERDZONY("Potwierdzony"),

    ODZUCONY("Odrzucony"),

    NIE_WYPELNIONY("Nie wypelniony"),
    ARCHIWOWANY("Archiwowany");

    public String getName() {
        return name;
    }

    private final String name;

    StatusProtokol(String name) {
        this.name = name;
    }
}
