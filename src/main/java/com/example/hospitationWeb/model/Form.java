package com.example.hospitationWeb.model;


import javax.persistence.*;

@Entity
@Table(name = "form")
public class Form {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    private Long hospitationId;


    // Strings
    private String opoznienieRozpoczeciaZajec;
    private String czySalaJestDostosowanaPrzyczyna;
    private String inneUwagi;
    private String prowadzacy;
    private String nazwaKursa;
    private String kod;
    private String formaDydaktyczna;
    private String sposobRealizacji;
    private String formaStudiow;
    private String termin;
    private String srodowisko;
    private String uzasadnienieOcenyKoncowej;
    private String wnioskiOrazZalecenia;

    //Booleans
    private Boolean czyZajeciaRozpoczelySiePunktualnie;
    private Boolean czySprawdzonoObecnoscStudentow;
    private Boolean czySalaJestDostosowana;
    private Boolean czyProwadzacywWeryfikuje;
    private Boolean czyTrescZgodnaZProgramemKursu;
    private Boolean czyUmozliwilDostep;


    // Doubles
    private Double koncowa;
    private Double tematCel;
    private Double wyjasnienie;
    private Double zaangazowanie;
    private Double inspiracja;
    private Double poprawneOdpowiedzi;
    private Double srodkiDydaktyczne;
    private Double poprawnyJezyk;
    private Double dynamikaGrupy;
    private Double atmosfera;
    private Double technicznesrodkiprzekazaniawiedzy;
    private Double aktualnaWiedza;
    private Double material;
    private Double umiejetnosciNauczania;
    private Double przyklady;
    private Double tempo;
    private Double przygotowanie;
    private Double zadania;
    private Double planowanie;
    private Double komentarzy;
    private Double dokumentacja;

    //Integers
    private Integer liczbaZapisanych;
    private Integer semestr;


    public Form(String opoznienieRozpoczeciaZajec) {
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
    }


    public Form() {

    }

    public Form(String opoznienieRozpoczeciaZajec, String czySalaJestDostosowanaPrzyczyna, String inneUwagi, String prowadzacy, String nazwaKursa, String kod, String formaDydaktyczna, String sposobRealizacji, String formaStudiow, String termin, String srodowisko, String uzasadnienieOcenyKoncowej, String wnioskiOrazZalecenia, Boolean czyZajeciaRozpoczelySiePunktualnie, Boolean czySprawdzonoObecnoscStudentow, Boolean czySalaJestDostosowana, Boolean czyProwadzacywWeryfikuje, Boolean czyTrescZgodnaZProgramemKursu, Boolean czyUmozliwilDostep, Double koncowa, Double tematCel, Double wyjasnienie, Double zaangazowanie, Double inspiracja, Double poprawneOdpowiedzi, Double srodkiDydaktyczne, Double poprawnyJezyk, Double dynamikaGrupy, Double atmosfera, Double technicznesrodkiprzekazaniawiedzy, Double aktualnaWiedza, Double material, Double umiejetnosciNauczania, Double przyklady, Double tempo, Double przygotowanie, Double zadania, Double planowanie, Double komentarzy, Double dokumentacja, Integer liczbaZapisanych, Integer semestr) {
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
        this.czySalaJestDostosowanaPrzyczyna = czySalaJestDostosowanaPrzyczyna;
        this.inneUwagi = inneUwagi;
        this.prowadzacy = prowadzacy;
        this.nazwaKursa = nazwaKursa;
        this.kod = kod;
        this.formaDydaktyczna = formaDydaktyczna;
        this.sposobRealizacji = sposobRealizacji;
        this.formaStudiow = formaStudiow;
        this.termin = termin;
        this.srodowisko = srodowisko;
        this.uzasadnienieOcenyKoncowej = uzasadnienieOcenyKoncowej;
        this.wnioskiOrazZalecenia = wnioskiOrazZalecenia;
        this.czyZajeciaRozpoczelySiePunktualnie = czyZajeciaRozpoczelySiePunktualnie;
        this.czySprawdzonoObecnoscStudentow = czySprawdzonoObecnoscStudentow;
        this.czySalaJestDostosowana = czySalaJestDostosowana;
        this.czyProwadzacywWeryfikuje = czyProwadzacywWeryfikuje;
        this.czyTrescZgodnaZProgramemKursu = czyTrescZgodnaZProgramemKursu;
        this.czyUmozliwilDostep = czyUmozliwilDostep;
        this.koncowa = koncowa;
        this.tematCel = tematCel;
        this.wyjasnienie = wyjasnienie;
        this.zaangazowanie = zaangazowanie;
        this.inspiracja = inspiracja;
        this.poprawneOdpowiedzi = poprawneOdpowiedzi;
        this.srodkiDydaktyczne = srodkiDydaktyczne;
        this.poprawnyJezyk = poprawnyJezyk;
        this.dynamikaGrupy = dynamikaGrupy;
        this.atmosfera = atmosfera;
        this.technicznesrodkiprzekazaniawiedzy = technicznesrodkiprzekazaniawiedzy;
        this.aktualnaWiedza = aktualnaWiedza;
        this.material = material;
        this.umiejetnosciNauczania = umiejetnosciNauczania;
        this.przyklady = przyklady;
        this.tempo = tempo;
        this.przygotowanie = przygotowanie;
        this.zadania = zadania;
        this.planowanie = planowanie;
        this.komentarzy = komentarzy;
        this.dokumentacja = dokumentacja;
        this.liczbaZapisanych = liczbaZapisanych;
        this.semestr = semestr;
    }

    public String getOpoznienieRozpoczeciaZajec() {
        return opoznienieRozpoczeciaZajec;
    }

    public void setOpoznienieRozpoczeciaZajec(String opoznienieRozpoczeciaZajec) {
        this.opoznienieRozpoczeciaZajec = opoznienieRozpoczeciaZajec;
    }

    public String getCzySalaJestDostosowanaPrzyczyna() {
        return czySalaJestDostosowanaPrzyczyna;
    }

    public void setCzySalaJestDostosowanaPrzyczyna(String czySalaJestDostosowanaPrzyczyna) {
        this.czySalaJestDostosowanaPrzyczyna = czySalaJestDostosowanaPrzyczyna;
    }

    public String getInneUwagi() {
        return inneUwagi;
    }

    public void setInneUwagi(String inneUwagi) {
        this.inneUwagi = inneUwagi;
    }

    public String getProwadzacy() {
        return prowadzacy;
    }

    public void setProwadzacy(String prowadzacy) {
        this.prowadzacy = prowadzacy;
    }

    public String getNazwaKursa() {
        return nazwaKursa;
    }

    public void setNazwaKursa(String nazwaKursa) {
        this.nazwaKursa = nazwaKursa;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getFormaDydaktyczna() {
        return formaDydaktyczna;
    }

    public void setFormaDydaktyczna(String formaDydaktyczna) {
        this.formaDydaktyczna = formaDydaktyczna;
    }

    public String getSposobRealizacji() {
        return sposobRealizacji;
    }

    public void setSposobRealizacji(String sposobRealizacji) {
        this.sposobRealizacji = sposobRealizacji;
    }

    public String getFormaStudiow() {
        return formaStudiow;
    }

    public void setFormaStudiow(String formaStudiow) {
        this.formaStudiow = formaStudiow;
    }

    public String getTermin() {
        return termin;
    }

    public void setTermin(String termin) {
        this.termin = termin;
    }

    public String getSrodowisko() {
        return srodowisko;
    }

    public void setSrodowisko(String srodowisko) {
        this.srodowisko = srodowisko;
    }

    public String getUzasadnienieOcenyKoncowej() {
        return uzasadnienieOcenyKoncowej;
    }

    public void setUzasadnienieOcenyKoncowej(String uzasadnienieOcenyKoncowej) {
        this.uzasadnienieOcenyKoncowej = uzasadnienieOcenyKoncowej;
    }

    public String getWnioskiOrazZalecenia() {
        return wnioskiOrazZalecenia;
    }

    public void setWnioskiOrazZalecenia(String wnioskiOrazZalecenia) {
        this.wnioskiOrazZalecenia = wnioskiOrazZalecenia;
    }

    public Boolean getCzyZajeciaRozpoczelySiePunktualnie() {
        return czyZajeciaRozpoczelySiePunktualnie;
    }

    public void setCzyZajeciaRozpoczelySiePunktualnie(Boolean czyZajeciaRozpoczelySiePunktualnie) {
        this.czyZajeciaRozpoczelySiePunktualnie = czyZajeciaRozpoczelySiePunktualnie;
    }

    public Boolean getCzySprawdzonoObecnoscStudentow() {
        return czySprawdzonoObecnoscStudentow;
    }

    public void setCzySprawdzonoObecnoscStudentow(Boolean czySprawdzonoObecnoscStudentow) {
        this.czySprawdzonoObecnoscStudentow = czySprawdzonoObecnoscStudentow;
    }

    public Boolean getCzySalaJestDostosowana() {
        return czySalaJestDostosowana;
    }

    public void setCzySalaJestDostosowana(Boolean czySalaJestDostosowana) {
        this.czySalaJestDostosowana = czySalaJestDostosowana;
    }

    public Boolean getCzyProwadzacywWeryfikuje() {
        return czyProwadzacywWeryfikuje;
    }

    public void setCzyProwadzacywWeryfikuje(Boolean czyProwadzacywWeryfikuje) {
        this.czyProwadzacywWeryfikuje = czyProwadzacywWeryfikuje;
    }

    public Boolean getCzyTrescZgodnaZProgramemKursu() {
        return czyTrescZgodnaZProgramemKursu;
    }

    public void setCzyTrescZgodnaZProgramemKursu(Boolean czyTrescZgodnaZProgramemKursu) {
        this.czyTrescZgodnaZProgramemKursu = czyTrescZgodnaZProgramemKursu;
    }

    public Boolean getCzyUmozliwilDostep() {
        return czyUmozliwilDostep;
    }

    public void setCzyUmozliwilDostep(Boolean czyUmozliwilDostep) {
        this.czyUmozliwilDostep = czyUmozliwilDostep;
    }

    public Double getKoncowa() {
        return koncowa;
    }

    public void setKoncowa(Double koncowa) {
        this.koncowa = koncowa;
    }

    public Double getTematCel() {
        return tematCel;
    }

    public void setTematCel(Double tematCel) {
        this.tematCel = tematCel;
    }

    public Double getWyjasnienie() {
        return wyjasnienie;
    }

    public void setWyjasnienie(Double wyjasnienie) {
        this.wyjasnienie = wyjasnienie;
    }

    public Double getZaangazowanie() {
        return zaangazowanie;
    }

    public void setZaangazowanie(Double zaangazowanie) {
        this.zaangazowanie = zaangazowanie;
    }

    public Double getInspiracja() {
        return inspiracja;
    }

    public void setInspiracja(Double inspiracja) {
        this.inspiracja = inspiracja;
    }

    public Double getPoprawneOdpowiedzi() {
        return poprawneOdpowiedzi;
    }

    public void setPoprawneOdpowiedzi(Double poprawneOdpowiedzi) {
        this.poprawneOdpowiedzi = poprawneOdpowiedzi;
    }

    public Double getSrodkiDydaktyczne() {
        return srodkiDydaktyczne;
    }

    public void setSrodkiDydaktyczne(Double srodkiDydaktyczne) {
        this.srodkiDydaktyczne = srodkiDydaktyczne;
    }

    public Double getPoprawnyJezyk() {
        return poprawnyJezyk;
    }

    public void setPoprawnyJezyk(Double poprawnyJezyk) {
        this.poprawnyJezyk = poprawnyJezyk;
    }

    public Double getDynamikaGrupy() {
        return dynamikaGrupy;
    }

    public void setDynamikaGrupy(Double dynamikaGrupy) {
        this.dynamikaGrupy = dynamikaGrupy;
    }

    public Double getAtmosfera() {
        return atmosfera;
    }

    public void setAtmosfera(Double atmosfera) {
        this.atmosfera = atmosfera;
    }

    public Double getTechnicznesrodkiprzekazaniawiedzy() {
        return technicznesrodkiprzekazaniawiedzy;
    }

    public void setTechnicznesrodkiprzekazaniawiedzy(Double technicznesrodkiprzekazaniawiedzy) {
        this.technicznesrodkiprzekazaniawiedzy = technicznesrodkiprzekazaniawiedzy;
    }

    public Double getAktualnaWiedza() {
        return aktualnaWiedza;
    }

    public void setAktualnaWiedza(Double aktualnaWiedza) {
        this.aktualnaWiedza = aktualnaWiedza;
    }

    public Double getMaterial() {
        return material;
    }

    public void setMaterial(Double material) {
        this.material = material;
    }

    public Double getUmiejetnosciNauczania() {
        return umiejetnosciNauczania;
    }

    public void setUmiejetnosciNauczania(Double umiejetnosciNauczania) {
        this.umiejetnosciNauczania = umiejetnosciNauczania;
    }

    public Double getPrzyklady() {
        return przyklady;
    }

    public void setPrzyklady(Double przyklady) {
        this.przyklady = przyklady;
    }

    public Double getTempo() {
        return tempo;
    }

    public void setTempo(Double tempo) {
        this.tempo = tempo;
    }

    public Double getPrzygotowanie() {
        return przygotowanie;
    }

    public void setPrzygotowanie(Double przygotowanie) {
        this.przygotowanie = przygotowanie;
    }

    public Double getZadania() {
        return zadania;
    }

    public void setZadania(Double zadania) {
        this.zadania = zadania;
    }

    public Double getPlanowanie() {
        return planowanie;
    }

    public void setPlanowanie(Double planowanie) {
        this.planowanie = planowanie;
    }

    public Double getKomentarzy() {
        return komentarzy;
    }

    public void setKomentarzy(Double komentarzy) {
        this.komentarzy = komentarzy;
    }

    public Double getDokumentacja() {
        return dokumentacja;
    }

    public void setDokumentacja(Double dokumentacja) {
        this.dokumentacja = dokumentacja;
    }

    public Integer getLiczbaZapisanych() {
        return liczbaZapisanych;
    }

    public void setLiczbaZapisanych(Integer liczbaZapisanych) {
        this.liczbaZapisanych = liczbaZapisanych;
    }

    public Integer getSemestr() {
        return semestr;
    }

    public void setSemestr(Integer semestr) {
        this.semestr = semestr;
    }
}

