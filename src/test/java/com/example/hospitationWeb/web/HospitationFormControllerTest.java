package com.example.hospitationWeb.web;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertTrue;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class HospitationFormControllerTest {

    @LocalServerPort
    private int port;

    TestRestTemplate restTemplate = new TestRestTemplate();

    HttpHeaders headers = new HttpHeaders();

    @Test
    void getHospitationResult() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/hospitationForm/"), HttpMethod.GET, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.CONNECTION).get(0);

        assertTrue(actual.contains("keep-alive"));
    }

    @Test
    void addToForm() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/addToForm/"), HttpMethod.POST, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.CONNECTION).get(0);

        assertTrue(actual.contains("keep-alive"));
    }

    @Test
    void hospitationResult() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/hospitationResult/"), HttpMethod.GET, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.CONNECTION).get(0);

        assertTrue(actual.contains("keep-alive"));
    }
    @Test
    void hospitationArchiwum() {
        HttpEntity<String> entity = new HttpEntity<String>(null, headers);

        ResponseEntity<String> response = restTemplate.exchange(
                createURLWithPort("/hospitationArchiwum/"), HttpMethod.GET, entity, String.class);

        String actual = response.getHeaders().get(HttpHeaders.CONNECTION).get(0);

        assertTrue(actual.contains("keep-alive"));
    }


    private String createURLWithPort(String uri) {
        return "http://localhost:" + port + uri;
    }
    @Test
    void getHospitationArchiwum() {
    }

    @Test
    void testGetHospitationArchiwum() {
    }
}