package com.example.hospitationWeb.service;

import com.example.hospitationWeb.model.Courses;
import com.example.hospitationWeb.model.Form;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import com.example.hospitationWeb.repository.CoursesRepository;
import com.example.hospitationWeb.repository.FormRepository;
import com.example.hospitationWeb.repository.HospitationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class HospitationFormServiceTest {

    @Autowired
    private HospitationRepository hospitationRepository;

    @Autowired
    private CoursesRepository coursesRepository;

    @Autowired
    private FormRepository formRepository;


    private HospitationFormService hospitationFormService;

    @BeforeEach
    void setUp() {
        hospitationFormService = new HospitationFormService(formRepository,hospitationRepository);
    }

    @Test
    void changeStatus() {
        //give
        Courses courses1 = new Courses(" Programowanie system. webowych", "INZ004361L ", "15", "Poniedziałek, 1705-1845, D-2, s. 127a", "Mgr inż. Arkadiusz Warzyński");
        coursesRepository.save(courses1);
        Hospitation hospitation = new Hospitation("hospitujacy", "hospitujacyDrugi", "hospitowany","20.05.2022", StatusProtokol.NIE_WYPELNIONY.getName(), "Letni2022" );
        hospitation.setCourses(courses1);
        hospitationRepository.save(hospitation);

        //when
        System.out.println(hospitationRepository.findAll());
        StatusProtokol exist = StatusProtokol.ARCHIWOWANY;
        hospitationFormService.changeStatus(3L,exist);
        Optional<Hospitation> hospitationExpected = hospitationRepository.findById(3L);

       // then
        assertThat(hospitationExpected.get().getStatus()).isEqualTo(exist.getName());
    }

    @Test
    void saveForm() {
        //give
        Courses courses1 = new Courses(" Programowanie system. webowych", "INZ004361L ", "15", "Poniedziałek, 1705-1845, D-2, s. 127a", "Mgr inż. Arkadiusz Warzyński");
        coursesRepository.save(courses1);
        Hospitation hospitation = new Hospitation("hospitujacy", "hospitujacyDrugi", "hospitowany","20.05.2022", StatusProtokol.NIE_WYPELNIONY.getName(), "Letni2022" );
        hospitation.setCourses(courses1);
        hospitationRepository.save(hospitation);

        //when
        StatusProtokol exist = StatusProtokol.ARCHIWOWANY;
        Form form = new Form("0","jest", "nie ma", "Dr inż. Bogumiła Hnatkowska","Projektowanie opragramowania", "INZ004360W","Project","stacjonarno","stacjo","Poniedziałek, 1705-1845, D-2, s. 127a","stacjo","Bardzo bardzo dobrze","Wnioski",true,true,true,true,true,true, 5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5.0,5,5);

        hospitationFormService.saveForm(form,3L, StatusProtokol.OCZEKUJE_NA_POTWIERDZENIE);
        Optional<Hospitation> hospitationExpected = hospitationRepository.findById(3L);

        // then
        assertThat(hospitationExpected.get().getForm()).isEqualTo(form);
    }

}