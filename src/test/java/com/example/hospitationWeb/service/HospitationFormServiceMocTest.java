package com.example.hospitationWeb.service;

import com.example.hospitationWeb.model.Courses;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import com.example.hospitationWeb.repository.CoursesRepository;
import com.example.hospitationWeb.repository.FormRepository;
import com.example.hospitationWeb.repository.HospitationRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class HospitationFormServiceMocTest {

    @Mock
    private HospitationRepository hospitationRepository;

    @Mock
    private CoursesRepository coursesRepository;

    @Mock
    private FormRepository formRepository;


    private HospitationFormService hospitationFormService;

    @BeforeEach
    void setUp() {
        hospitationFormService = new HospitationFormService(formRepository,hospitationRepository);
    }

    @Test
    void changeStatus() {
        //give
        Courses courses1 = new Courses(" Programowanie system. webowych", "INZ004361L ", "15", "Poniedziałek, 1705-1845, D-2, s. 127a", "Mgr inż. Arkadiusz Warzyński");
        coursesRepository.save(courses1);
        Hospitation hospitation = new Hospitation("hospitujacy", "hospitujacyDrugi", "hospitowany","20.05.2022", StatusProtokol.NIE_WYPELNIONY.getName(), "Letni2022" );
        hospitation.setCourses(courses1);
        hospitationRepository.save(hospitation);

        verify(hospitationRepository).save(hospitation);
        //when
        System.out.println(hospitationRepository.findAll());
        StatusProtokol exist = StatusProtokol.ARCHIWOWANY;
      //  hospitationFormService.changeStatus(1L,exist);
  //      Optional<Hospitation> hospitationExpected = hospitationRepository.findById(5L);

        //then
     //   assertThat(hospitationExpected.get().getStatus()).isEqualTo(exist.getName());
    }

    @Test
    void changeStatusThrowNull() {

        //when
        StatusProtokol exist = StatusProtokol.ARCHIWOWANY;

        given(hospitationRepository.getOne(1L)).willThrow(NullPointerException.class);

        assertThatThrownBy(() ->  hospitationFormService.changeStatus(1L,exist)).isInstanceOf(NullPointerException.class);
    }

}