package com.example.hospitationWeb.repository;

import com.example.hospitationWeb.model.Courses;
import com.example.hospitationWeb.model.Hospitation;
import com.example.hospitationWeb.model.StatusProtokol;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class HospitationRepositoryTest {

    @Autowired
    private HospitationRepository hospitationRepository;

    @Autowired
    private CoursesRepository coursesRepository;


    @Test
    void itShouldCheckIfHospitationExistsBySemestrBeforAdd() {
        //give
        //when
        Integer expected = hospitationRepository.findAllBySemestr("Letni2022").size();
        Integer exist = 2;
        //then
        assertThat(expected).isEqualTo(exist);
    }

    @Test
    void itShouldCheckIfHospitationExistsBySemestrAfterAdd() {
        //give
        Courses courses1 = new Courses();
        courses1.setName(" Programowanie system. webowych");
        courses1.setCourseName("INZ004361L ");
        courses1.setCountStudents("15");
        courses1.setTermAndPlace("Poniedziałek, 1705-1845, D-2, s. 127a");
        courses1.setPersonHospitation("Mgr inż. Arkadiusz Warzyński");
        coursesRepository.save(courses1);
        Hospitation hospitation = new Hospitation();
        hospitation.setHospitujacyPierwszyId("hospitujacy");
        hospitation.setHospitujacyDrugiId("hospitowany");
        hospitation.setHospitationDate("20.05.2022");
        hospitation.setStatus(StatusProtokol.NIE_WYPELNIONY.getName());
        hospitation.setSemestr("Letni2022");
        hospitation.setCourses(courses1);
        hospitationRepository.save(hospitation);
        //when
        Integer expected = hospitationRepository.findAllBySemestr("Letni2022").size();
        Integer exist = 3;
        //then
        assertThat(expected).isEqualTo(exist);
    }

    @Test
    void findAllByStatusNieWypelniono() {

        //when
        Integer expected = hospitationRepository.findAllByStatus(StatusProtokol.NIE_WYPELNIONY.getName()).size();
        Integer exist = 2;
        //then
        assertThat(expected).isEqualTo(exist);
    }

    @Test
    void findAllByStatusWhichNotExist() {

        //when
        List<Hospitation> expected = hospitationRepository.findAllByStatus("status ktorego nie mamy");
        //then
        assertThat(expected).isEqualTo(Collections.emptyList());
    }

}